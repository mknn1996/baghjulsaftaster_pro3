
#include <avr/io.h>
#include <avr/interrupt.h>


#define INTERRUPT_PIN PCINT2  
#define INT_PIN PB2          
#define LED_PIN PB1          
#define PULSES_PER_ROTATION 20.00 
#define WHEEL_CIRCUMFERENCE 875.00 
#define HOUR_FACTOR 3600

double time = 0.0;
double speed = 0.0;
int c;
double last = 0.0;

#define PCINT_VECTOR PCINT0_vect     

void setup() {
    
  Serial.begin(115200);

  /* Turn on LED so user knows we are on. */
  pinMode(LED_PIN, OUTPUT);        
  digitalWrite(LED_PIN, HIGH);      

  cli();                            // Disable interrupts.
  PCMSK |= (1 << INTERRUPT_PIN);    // Set pin/bit in interrupt register.
  GIMSK |= (1 << PCIE);             // Enable change interrupt.
  pinMode(INT_PIN, INPUT_PULLUP);   // Pull up the pin to stabilize.
  sei();                            // Enable interrupts.

}


ISR(PCINT_VECTOR)
{
/* Runs when pin status changes */  
  if( digitalRead(INT_PIN) == HIGH ) {  
    c++;
    if( c >= PULSES_PER_ROTATION ) {
      time = (double)micros() - last;
      speed = (WHEEL_CIRCUMFERENCE/time) * HOUR_FACTOR; // km/hr.
      Serial.println(speed, 2);
      last = micros();
      c = 0;
    }
  }
}


void loop() {

}
